# Taula de continguts
- [Com interactuar amb IRC des de Matrix][matrix-irc] 13/12/2020
- [Què és Salsa?][que-es-salsa] 21/12/2020
- [Què és Debian Social][que-es-d-social] 28/12/2020
- [Què és Debian Academy][que-es-d-academy] 17/01/2021

[matrix-irc]: content/com-interactuar-amb-irc-des-de-matrix.md
[que-es-salsa]: content/que-es-salsa.md
[que-es-d-social]: content/que-es-debian-social.md
[que-es-d-academy]: content/que-es-debian-academy.md

# Descripció
Aquest repositori conté diferents articles o píndoles creats per i per a la
comunitat de [DebianCat][debiancat], la comunitat d'usuaris de Debian de parla
catalana.

Si tens dubtes, posa't en contacte a través dels canals habituals.

[debiancat]: https://wiki.debian.org/LocalGroups/DebianCat

# Com col·laborar-hi
En primer lloc et convidem a llegir el fitxer [CONTRIBUTING.md](CONTRIBUTING.md)
on es descriuen uns mínims que permeten que tothom qui hi col·labori s'hi trobi
còmode.

Per col·laborar no cal ser membre del projecte, només un usuari a
[Salsa][salsa]. De totes maneres, si volguessis formar-ne part de forma
explícita, et convidem a fer la petició a la [llista de correu][ml], indicant
quin és el teu usuari de Salsa i una breu descripció de com t'agradaria
col·laborar-hi; d'aquesta manera ens coneixerem tots :-)

[salsa]: https://salsa.debian.org
[ml]: https://lists.debian.org/debian-user-catalan/

## Publicar una píndola
Els documents (les píndoles) estan en format [Markdown][md] (un llenguatge de
marques similar a l'HTML molt senzill d'aprendre). Redacta'n tantes com vulguis
seguint els passos que trobaràs a continuació i crea una *Merge request* quan
estiguin llestes. Si tens dubtes amb el funcionament de git, digue-ho i segur
que trobarem algú que pugui ajudar-te.

1. Descarrega't una còpia del repositori (pots fer un *fork*)
2. Crea una branca local
3. Crear el contingut al directori `content/`
4. Afegir un enllaç a aquest fitxer (`README.md`) amb una descripció, si vols.
5. Fes els *commits* i el corresponent `git push`
6. Crear una merge request

Què passarà llavors?

1. Algú verificarà que tot estigui en ordre (per allò que quatre ulls hi veuen
més que dos; és el procés habitual).
2. Un cop tot estigui tot validat, la teva aportació es fusionarà amb la resta.
3. Podràs veure la teva col·laboració a la [pàgina principal][pindoles].
4. Addicionalment es llençarà un procés de forma automàtica que publicarà la
teva aportació a [https://debiancat-team.pages.debian.net/pindoles][gl-pages]

[md]: https://ca.wikipedia.org/wiki/Markdown
[pindoles]: https://salsa.debian.org/debiancat-team/pindoles
[gl-pages]: https://debiancat-team.pages.debian.net/pindoles

## Altres maneres
- Si no escrius, sempre pots proposar temes a tractar, suggeriments o
modificacions a aquelles píndoles que hagin quedat desfassades o tinguin
incorreccions creant una *[issue][gl-issue]*.
- Fes-ne difusió: comparteix el contingut enllaçant
[https://debiancat-team.pages.debian.net/pindoles][gl-pages]
- Posa aquí la teva idea

[gl-issue]: https://salsa.debian.org/debiancat-team/pindoles/-/issues
[gl-pages]: https://debiancat-team.pages.debian.net/pindoles
